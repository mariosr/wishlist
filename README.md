# WishList Project

This is a Spring Boot project developed in Kotlin for managing a wishlist of products.
- AS IS
![img_2.png](img_2.png)
- FUTURE
![img_1.png](img_1.png)
-SWAGGER (http://localhost:8888/wishlist/swagger-ui/index.html)
- ![img.png](img.png)

## Prerequisites

Before you start, ensure that you have the following installed on your machine:

- [Docker](https://docs.docker.com/get-docker/)
- [Docker Compose](https://docs.docker.com/compose/install/)
- Java Development Kit (JDK) 17 or later

## How to Run

Follow the steps below to run the Spring Boot Kotlin project along with Docker Compose.

1. **Clone the Repository**

   ```bash
   git clone <repository-url>
   cd project-directory

2. **Build the project**

   ```bash
   maven clean install

3. **Run docker compose**

   ```bash
   docker-compose up

4. **Run the project**

   ```bash
   spring-boot:run
