package com.magalu.wishlist.service

import com.magalu.wishlist.exception.WishlistLimitExceededException
import com.magalu.wishlist.model.domain.ProductDto
import org.junit.jupiter.api.extension.ExtendWith
import com.magalu.wishlist.model.entity.Product
import com.magalu.wishlist.repository.ProductRepository
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.http.HttpStatus
import java.util.Optional

@ExtendWith(MockitoExtension::class)
class ProductServiceTest {

    @Mock
    private lateinit var productRepository: ProductRepository

    @InjectMocks
    private lateinit var productService: ProductService

    @Test
    fun testGetAllProducts() {
        val productList = listOf(Product(1, "Product 1"), Product(2, "Product 2"))

        // Mock the repository method
        Mockito.`when`(productRepository.findAll()).thenReturn(productList)

        // Invoke the service method
        val result = productService.getAllProducts()

        // Verify the result
        Assertions.assertEquals(productList.size, result.size)
    }

    @Test
    fun testGetProductById() {
        val productId = 1
        val productDto = ProductDto(productId, "Test Product")

        // Mock the repository method
        Mockito.`when`(productRepository.findById(productId)).thenReturn(Optional.of(
                Product(productDto.id, productDto.name)
        ))

        // Invoke the service method
        val result = productService.getProductById(productId)

        // Verify the result
        Assertions.assertEquals(Optional.of(productDto).get().id, result.get().id)
    }

    @Test
    fun testAddProduct() {
        val product = Product(1, "Test Product")

        // Mock the repository method
        Mockito.`when`(productRepository.save(product)).thenReturn(product)

        // Invoke the service method
        val result = productService.addProduct(ProductDto(product.id, product.name))

        // Verify the result
        Assertions.assertEquals("Product with ID ${product.id} added to the wishlist.", result.body)
    }

    @Test
    fun testAddProductExceedingLimit() {
        val product = ProductDto(1, "Test Product")

        // Mock the repository method
        Mockito.`when`(productRepository.count()).thenReturn(20L)

        // Invoke the service method
        Assertions.assertThrows(WishlistLimitExceededException::class.java) {
            productService.addProduct(product)
        }
    }


    @Test
    fun testRemoveProduct() {
        val productId = 1

        // Invoke the service method
        productService.removeProduct(productId)

        // Verify that the repository method was called with the correct argument
        Mockito.verify(productRepository, Mockito.times(1)).deleteById(productId)
    }
}