package com.magalu.wishlist.controller

import com.magalu.wishlist.exception.WishlistLimitExceededException
import com.magalu.wishlist.model.domain.ProductDto
import com.magalu.wishlist.service.ProductService
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.http.ResponseEntity
import java.util.Optional

@ExtendWith(MockitoExtension::class)
class WishListControllerTest {

    @Mock
    private lateinit var productService: ProductService

    @InjectMocks
    private lateinit var wishListController: WishListController

    @Test
    fun testAddProduct() {
        val product = ProductDto(1, "Test Product")
        val response = ResponseEntity.ok("Product with ID ${product.id} added to the wishlist.")
        // Mock the service method
        Mockito.`when`(productService.addProduct(product)).thenReturn(response)

        // Invoke the controller method directly
        val result = wishListController.addProduct(product)

        // Verify the result
        Assertions.assertEquals("Product with ID 1 added to the wishlist.", result.body)
    }

    @Test
    fun testAddProductWithException() {
        val product = ProductDto(1, "Test Product")
        val exceptionMessage = "Simulated exception message"
        val exception = WishlistLimitExceededException(exceptionMessage)

        // Mock the service method to throw an exception
        Mockito.`when`(productService.addProduct(product)).thenThrow(exception)

        // Invoke the controller method directly
        Assertions.assertThrows(WishlistLimitExceededException::class.java) {
            wishListController.addProduct(product)
        }
    }

    @Test
    fun testRemoveProduct() {
        val productId = 1

        // Invoke the controller method directly
        val result = wishListController.removeProduct(productId)

        // Verify the result
        Assertions.assertEquals("Product removed from wishlist (ID: 1)", result.body)
    }

    @Test
    fun testGetAllProducts() {
        val productList = listOf(ProductDto(1, "Product 1"), ProductDto(2, "Product 2"))

        // Mock the service method
        Mockito.`when`(productService.getAllProducts()).thenReturn(productList)

        // Invoke the controller method directly
        val result = wishListController.getAllProducts()

        // Verify the result
        Assertions.assertEquals(productList, result)
    }

    @Test
    fun testGetProductById() {
        val product = ProductDto(1, "Test Product")

        // Mock the service method
        Mockito.`when`(productService.getProductById(1)).thenReturn(Optional.of(product))

        // Invoke the controller method directly
        val result = wishListController.getProductById(1)

        // Verify the result
        Assertions.assertNotNull(result.body)
    }
}
