package com.magalu.wishlist

import io.swagger.v3.oas.annotations.OpenAPIDefinition
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@OpenAPIDefinition
@SpringBootApplication
class WishlistApplication

fun main(args: Array<String>) {
    runApplication<WishlistApplication>(*args)
}
