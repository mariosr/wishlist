package com.magalu.wishlist.repository

import com.magalu.wishlist.model.entity.Product
import org.springframework.data.jpa.repository.JpaRepository

interface ProductRepository : JpaRepository<Product, Int>
