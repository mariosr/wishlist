package com.magalu.wishlist.exception

data class ApiError(val message: String)