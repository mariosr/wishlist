package com.magalu.wishlist.exception

class WishlistLimitExceededException(message: String) : RuntimeException(message)