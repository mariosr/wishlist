package com.magalu.wishlist.exception

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice

@RestControllerAdvice
class RestControllerAdviceExceptionHandler {

    private val log: Logger = LoggerFactory.getLogger(RestControllerAdviceExceptionHandler::class.java)

    @ExceptionHandler(MethodArgumentNotValidException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleMethodArgumentNotValid(ex: MethodArgumentNotValidException): ApiError {
        val errorMessage = "Error on validation: ${ex.message}"
        log.error(errorMessage, ex)
        return ApiError(errorMessage)
    }

    @ExceptionHandler(Exception::class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    fun handleGenericException(ex: Exception): ApiError {
        val errorMessage = "An internal server error occurred: ${ex.localizedMessage}"
        log.error(errorMessage, ex)
        return ApiError(errorMessage)
    }

    @ExceptionHandler(WishlistLimitExceededException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleWishlistLimitExceededException(ex: WishlistLimitExceededException): ApiError {
        val errorMessage = "A bad request error occurred: ${ex.localizedMessage}"
        log.warn(errorMessage)
        return ApiError(errorMessage)
    }
}
