package com.magalu.wishlist.service

import com.magalu.wishlist.exception.WishlistLimitExceededException
import com.magalu.wishlist.model.domain.ProductDto
import com.magalu.wishlist.model.entity.Product
import com.magalu.wishlist.repository.ProductRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.util.Optional

/**
 * Service class to handle operations related to products in the wishlist.
 *
 * @property productRepository The repository for accessing and managing product data.
 */
@Service
class ProductService @Autowired constructor(private val productRepository: ProductRepository) {

    private val log = LoggerFactory.getLogger(ProductService::class.java)

    /**
     * Retrieves all products from the wishlist.
     *
     * @return A list of [ProductDto] objects representing products in the wishlist.
     */
    fun getAllProducts(): List<ProductDto> {
        log.info("Retrieving all products from the wishlist.")
        val products: List<Product> = productRepository.findAll()

        return products.map { product ->
            ProductDto(product.id, product.name)
        }
    }

    /**
     * Retrieves a product from the wishlist by its ID.
     *
     * @param productId The ID of the product to retrieve.
     * @return An [Optional] containing the [ProductDto] if found, or empty if not.
     */
    fun getProductById(productId: Int): Optional<ProductDto> {
        log.info("Retrieving product with ID {} from the wishlist.", productId)
        return productRepository.findById(productId).map { product ->
            ProductDto(product.id, product.name)
        }
    }

    /**
     * Adds a product to the wishlist.
     *
     * @param product The product to be added.
     * @return A [ResponseEntity] with a success message if the product is added,
     *         or a bad request status with an error message if adding the product is not allowed.
     */
    fun addProduct(productDto: ProductDto): ResponseEntity<String> {
        log.info("Attempting to add product with ID {} to the wishlist.", productDto.name)
        return if (productRepository.count() < 20) {
            val product = productRepository.save(Product(productDto.id, productDto.name))
            log.info("Product with ID {} added to the wishlist.", product.id)
            ResponseEntity.ok("Product with ID ${product.id} added to the wishlist.")
        } else {
            val errorMessage = "Cannot add more than 20 products to the wishlist."
            log.warn(errorMessage)
            throw WishlistLimitExceededException(errorMessage)
        }
    }

    /**
     * Removes a product from the wishlist by its ID.
     *
     * @param productId The ID of the product to remove.
     */
    fun removeProduct(productId: Int) {
        log.info("Attempting to remove product with ID {} from the wishlist.", productId)
        productRepository.deleteById(productId)
        log.info("Product with ID {} removed from the wishlist.", productId)
    }
}
