package com.magalu.wishlist.model.domain

import lombok.Data


@Data
class ProductDto(
        val id: Int,
        val name: String
)