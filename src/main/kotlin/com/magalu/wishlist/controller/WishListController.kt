package com.magalu.wishlist.controller

import com.magalu.wishlist.model.domain.ProductDto
import com.magalu.wishlist.service.ProductService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
 * Controller class to handle HTTP requests related to products in the wishlist.
 *
 * @property productService The service for handling business logic related to products.
 */
@RestController
@RequestMapping("/product")
class WishListController @Autowired constructor(private val productService: ProductService) {

    private val log = LoggerFactory.getLogger(WishListController::class.java)

    /**
     * Adds a product to the wishlist.
     *
     * @param product The product to be added.
     * @return A [ResponseEntity] with a success message if the product is added,
     *         or a server error status with an error message if an exception occurs.
     */
    @PostMapping
    fun addProduct(@RequestBody product: ProductDto): ResponseEntity<String> {
        log.info("Received request to add product to the wishlist.")
        val responseEntity = productService.addProduct(product)
        log.info(responseEntity.body)
        return responseEntity
    }

    /**
     * Removes a product from the wishlist by its ID.
     *
     * @param productId The ID of the product to remove.
     * @return A [ResponseEntity] with a success message if the product is removed,
     *         or a server error status with an error message if an exception occurs.
     */
    @DeleteMapping("/{productId}")
    fun removeProduct(@PathVariable productId: Int): ResponseEntity<String> {
        log.info("Received request to remove product from the wishlist (ID: {}).", productId)
        productService.removeProduct(productId)
        val message = "Product removed from wishlist (ID: $productId)"
        log.info(message)
        return ResponseEntity.ok(message)
    }

    /**
     * Retrieves all products from the wishlist.
     *
     * @return A list of [ProductDto] objects representing products in the wishlist.
     */
    @GetMapping("/all")
    fun getAllProducts(): List<ProductDto> {
        log.info("Received request to retrieve all products from the wishlist.")
        return productService.getAllProducts()
    }

    /**
     * Retrieves a product from the wishlist by its ID.
     *
     * @param productId The ID of the product to retrieve.
     * @return A ProductDto found.
     */
    @GetMapping("/{productId}")
    fun getProductById(@PathVariable productId: Int): ResponseEntity<ProductDto> {
        log.info("Received request to retrieve product from the wishlist (ID: {}).", productId)
        val productOptional = productService.getProductById(productId)
        return productOptional.map { product ->
            log.info("Product found in wishlist: ${product.name} (ID: ${product.id})")
            ResponseEntity.ok(ProductDto(product.id, product.name))
        }.orElseGet {
            log.info("Product with ID $productId not found in the wishlist")
            ResponseEntity.noContent().build()
        }
    }
}
